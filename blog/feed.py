# -*- coding: utf-8 -*-
from django.contrib.syndication.views import Feed
from blog.models import Entrada

class UltimasEntradas(Feed):
    titulo = "Totes Amb Burjassot Blog"
    link = "/feed/"
    descripcion = "Ultimas Entradas"

    def items(self):
        return Entrada.objects.published()[:5]