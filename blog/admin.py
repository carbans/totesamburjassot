from django.contrib import admin
from blog.models import Categoria, Entrada, Comentario, Etiqueta


#class EntradaAdmin(admin.ModelAdmin):
#    list_display = ['titulo', 'descripcion']
#    list_filter = []
class EntradaAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('titulo',)}
    list_display = ('titulo', 'slug', 'fecha_creacion', 'categoria', 'autor', 'publicada')
    list_filter = ('fecha_creacion',)
    date_hierarchy = 'fecha_creacion'
    search_fields = ('titulo',)


class CategoriaAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('nombre',)}
    list_display = ('nombre', 'slug')


class ComentarioAdmin(admin.ModelAdmin):
    list_display = ('entrada', 'autor', 'email', 'texto', 'aprovado')
    list_filter = ('fecha_creacion',)


class EtiquetaAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('nombre',)}
    list_display = ('nombre',)

admin.site.register(Entrada, EntradaAdmin)
admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Comentario, ComentarioAdmin)
admin.site.register(Etiqueta, EtiquetaAdmin)
#admin.site.register(Entrada)
#admin.site.register(Categoria)
