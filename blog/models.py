from __future__ import unicode_literals
from django.template.defaultfilters import slugify
from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User


class Categoria(models.Model):
    nombre = models.CharField(max_length=200)
    slug = models.SlugField(unique=True, max_length=255)
#Para compatibilidad con python 3 deberia aparecer str

    def __unicode__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.nombre)
        super(Categoria, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Categoria"
        verbose_name_plural = "Categorias"
        ordering = ["-nombre"]


class Etiqueta(models.Model):
    nombre = models.CharField(max_length=200)
    slug = models.SlugField(unique=True, max_length=255)

    def __unicode__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.nombre)
        super(Etiqueta, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Etiqueta"
        verbose_name_plural = "Etiquetas"
        ordering = ["-nombre"]


class Entrada(models.Model):
    titulo = models.CharField(max_length=200)
    slug = models.SlugField(unique=True, max_length=255)
    descripcion = models.CharField(max_length=200)
    contenido = RichTextField()
    categoria = models.ForeignKey(Categoria)
    etiquetas = models.ManyToManyField(Etiqueta)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    publicada = models.BooleanField(default=True)
    autor = models.ForeignKey(User)
    #imagen = models.ImageField(upload_to='entradas/%Y/%m/%d', default='')

    def __unicode__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.titulo)
        super(Entrada, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Entrada"
        verbose_name_plural = "Entradas"
        ordering = ["-fecha_creacion"]


class Comentario(models.Model):
    entrada = models.ForeignKey('blog.Entrada', related_name='comentarios')
    autor = models.CharField(max_length=200)
    email = models.EmailField(max_length=255, default="", null=False)
    texto = models.TextField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    aprovado = models.BooleanField(default=False)

    def aprobar(self):
        self.aprovado = True
        self.save()

    def __unicode__(self):
        return self.texto

    class Meta:
        verbose_name = "Comentario"
        verbose_name_plural = "Comentarios"
        ordering = ["-fecha_creacion"]