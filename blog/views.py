#from django.http import HttpResponse
from django.shortcuts import render
from blog.models import Entrada


def vista_index(request):
    entradas = Entrada.objects.all()[:5]
    return render(request, 'index.html', {'entradas': entradas})


