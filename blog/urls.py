from django.conf.urls import url
from blog import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^$', views.vista_index),
    #url(r'^(?P<slug>\S+)$', views.vista_entrada),
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)